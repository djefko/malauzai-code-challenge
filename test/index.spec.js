const assert = require('assert');

describe(
  'Module',
  function ()
  {
    const config = require(process.env.npm_package_config_file);

    var nearby;

    it(
      'should be able to load without errors',
      function ()
      {
        nearby = require('../src/index');
      }
    );

    it(
      'should return a function',
      function ()
      {
        assert.strictEqual(typeof nearby, 'function');
      }
    );

    it(
      'should have a config file environment variable',
      function ()
      {
        assert.ok(process.env.npm_package_config_file);
      }
    );

    it(
      'should load the config file',
      function ()
      {
        assert.ok(config);
        assert.strictEqual(typeof config, 'object');
      }
    );

    describe(
      'config',
      function ()
      {
        it(
          'should have a list of customers',
          function ()
          {
            assert.ok(config.customers);
            assert.strictEqual(typeof config.customers, 'object');
          }
        );

        for (const k in config.customers)
        {
          const conf = config.customers[k];

          describe(
            k,
            function ()
            {
              it(
                'should have an API key',
                function ()
                {
                  assert.ok('apikey' in conf);
                  assert.ok(conf.apikey.length);
                }
              );

              it(
                'should have a preferred language',
                function ()
                {
                  assert.ok('language' in conf);
                  assert.strictEqual(conf.language.length, 2);
                }
              );

              it(
                'should have a preferred output format',
                function ()
                {
                  assert.ok('output' in conf);
                  assert.ok(
                    ['json', 'xml']
                    .includes(conf.output)
                  );
                }
              );

              it(
                'should have a preferred number of returned nearby locations',
                function ()
                {
                  assert.ok('totalLocations' in conf);
                  assert.strictEqual(
                    typeof conf.totalLocations,
                    'number'
                  );
                  assert.ok(conf.totalLocations > 0);
                }
              );

              it(
                'should have a preferred type of nearby places',
                function ()
                {
                  assert.ok('type' in conf);
                  assert.ok(conf.type.length > 0);
                }
              );
            }
          );
        }
      }
    );

    describe(
      'function',
      function ()
      {
        for (const k in config.customers)
        {
          const conf = config.customers[k];

          describe(
            `for ${k}`,
            function ()
            {
              const results = nearby(k, 46.0646, 118.3430);

              this.timeout(60 * 1000);

              it(
                'should return a Promise',
                function ()
                {
                  assert.ok(results instanceof Promise);
                }
              );

              it(
                'should succeed',
                function ()
                {
                  return results;
                }
              );

              it(
                'results should be an array',
                async function ()
                {
                  let ret = await results;

                  assert.ok(ret instanceof Array);
                }
              );

              it(
                'without parameters should fail',
                function ()
                {
                  return assert.rejects(nearby());
                }
              );

              it(
                'without latitude should fail',
                function ()
                {
                  return assert.rejects(nearby(k, undefined, 118.3430));
                }
              );

              it(
                'without longitude should fail',
                function ()
                {
                  return assert.rejects(nearby(k, 46.0646));
                }
              );

              it(
                'returns no more than the given maximum',
                async function ()
                {
                  const limited = await nearby(
                        k, 46.0646, 118.3430, undefined, 2
                      );

                  assert.ok(limited.length <= 2);
                }
              );

              it(
                'accepts an output parameter of "xml" and returns appropriately',
                async function ()
                {
                  let isXML = require('is-xml');

                  assert.ok(
                    isXML(
                      await nearby(
                        k, 46.0646, 118.3430, undefined, undefined, 'xml'
                      )
                    )
                  );
                }
              );

              it(
                'accepts an output parameter of "json" and returns appropriately',
                async function ()
                {
                  assert.ok(
                    await nearby(
                      k, 46.0646, 118.3430, undefined, undefined, 'json'
                    )
                    instanceof Array
                  );
                }
              );

              it(
                'accepts a type parameter and only returns results with the given type',
                async function ()
                {
                  const typed = await nearby(
                        k, 46.0646, 118.3430,
                        {
                          type: conf.type
                        }
                      );

                  assert.strictEqual(
                    typed.filter(
                      (itm) => conf.type == 'all'
                        || itm.types.includes(conf.type)
                    ).length,
                    typed.length
                  );
                }
              );
            }
          );
        }
      }
    );
  }
);
