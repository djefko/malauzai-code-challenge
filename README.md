# @jefko/nearby

A simple proxy module for the Google Places Search API which uses a separate Google API key per customer from a config file.

## Installation
Install with NPM.
```bash
npm install
```

## Usage
### Thenable
```javascript
const nearby = require('@jefko/nearby');

let customer = 'Paris FCU',
    lat = 48.864716,
    long = 2.349014,
    opts = {
      type: 'atm'
    };

nearby(customer, lat, long, opts)
.then(
  function (results)
  {
    // Utilize the results....
  }
)
.catch(
  function (err)
  {
    // Handle errors....
  }
);
```

### Async/Await
```javascript
async function getNearbyLocations()
{
  const nearby = require('@jefko/nearby');

  let customer = 'Paris FCU',
      lat = 48.864716,
      long = 2.349014,
      max = 40,
      opts = {
        type: 'bank'
      },
      results;

  try
  {
    results = await nearby(
      customer, lat, long, opts, max
    );
  } catch (e) {
    // Handle errors....
  }

  // Utilize the results....
}
```

### XML output
```javascript
...
nearby(customer, lat, long, opts, max, 'xml')
.then(
  function (results)
  {
    // Utilize the results....
  }
)
.catch(
  function (err)
  {
    // Handle errors....
  }
);
```

### Copyright
(c) 2020 Daniel Jeffery. All right reserved.
