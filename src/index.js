/**
 * @description A simple proxy module for the Google Places Search API
 * which uses a separate Google API key per customer from a config file.
 *
 * @author  Daniel Jeffery <malauzai@danieljeffery.me>
 *
 * @required  npm_package_config_file environment variable to load a JSON
 * config file.
 */

const config = require(process.env.npm_package_config_file),
    maps = require('@google/maps'),
    converter = require('xml-js');

/**
 * @description Recursive function for collecting paginated results from
 * Google's Places Search API.
 *
 * @param client  GoogleMapsClient  The client for making Google Places
 *                Search API calls.
 * @param opts  Object  The options/parameters object to pass to the API.
 * @param max Integer The maximum number of requests to return.
 *
 * @return  Array An array of locations from the API.
 */
async function recurse(client, opts, max = 20)
{
  // If max is <= 0, return an empty array.
  if (!max) return [];

  let response = await client.places(opts).asPromise(),
      results = response.json.results;

  // Results are paginated.
  // "next_page_token" only exists if more results can be retrieved.
  if ('next_page_token' in response.json)
  {
    opts.pagetoken = response.json.next_page_token;

    // Concatenate the results, reducing max by Google's max results.
    /**
     * Array.prototype.concat() is used instead of ES6 Spread
     * because we're adding to an existing array of unknown size.
     *
     * The following could be used, if desired:
     * results = [...results, ...await recurse(client, opts, max - 20)];
     */
    results = results.concat(
      await recurse(client, opts, max - 20)
    );
  }

  return results.slice(0, max); // Output no more than the maximum results.
}

/**
 * @description The default exported function for proxying requests to the
 * Google Places Search API with a customer specific API key.
 *
 * @param customer  String  The name of the customer. Must match the config key.
 * @param lat Number  The latitude of the requested search.
 * @param long  Number  The longitude of the requested search.
 * @param opts  Object  An object of optional parameters to pass to the API.
 * @param max Integer The maximum number of locations to return.
 * @param output  ENUM('json', 'xml') The output format of the results.
 *
 * @return  Array An array of locations from the API.
 */
module.exports = async function nearby(
    customer, lat, long, opts = {}, max = 20, output = 'json'
  )
{
  // Customer, Latitude, and Longitude are required.
  if (!customer || !(customer in config.customers))
    throw new Error('A valid customer is required.');

  if (typeof lat != 'number')
    throw new Error('Latitude is required.');

  if (typeof long != 'number')
    throw new Error('Longitude is required.');

  // Load the GoogleMapsClient.
  const client = maps.createClient(
        {
          key: config.customers[customer].apikey, // Use the customer's API key.
          Promise: Promise
        }
      );

  if (typeof opts != 'object')
    opts = {};

  opts.query = customer;
  opts.location = `${lat},${long}`;
  opts.radius = 1609.34;

  // Retreive un-paginated results.
  let results = await recurse(client, opts, max);

  // If the XML output format is not requested, return the results.
  if (output.toLowerCase() != 'xml')
    return results;

  // Convert the JS results array to XML with a declaration.
  return converter.js2xml(
    {
      '_declaration': {
        '_attributes': {
          encoding: 'utf-8',
          version: '1.0'
        }
      },
      'result': results
    },
    {
      compact: true
    }
  );
};
